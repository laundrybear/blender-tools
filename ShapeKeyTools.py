import bpy

class LAUNDRYBEAR_PT_ShapeKeyToolPanel(bpy.types.Panel):
    bl_label = "Shape Key Tools"
    bl_idname = "LAUNDRYBEAR_PT_ShapeKeyTools"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Laundry Bear"
         
    def draw(self, context):
        layout = self.layout
        
        row = layout.row()
        row.label(icon = "SORTALPHA")
        row.operator("laundrybear.shape_key_sort")

class LAUNDRYBEAR_OT_ShapeKeySort(bpy.types.Operator):
    """Sort Shape Keys Alphabetically"""
    bl_label = "Sort Shape Keys"
    bl_idname = "laundrybear.shape_key_sort"

    @classmethod
    def poll(cls, context):
        obj = context.object
        
        if obj is not None:
            if obj.type == "MESH":
                return True
        return False

    def execute(self, context):
        ob = bpy.context.object
        if ob.data.shape_keys.key_blocks:
            skeys = ob.data.shape_keys.key_blocks
        else: 
            return {'CANCELLED'}
        
        if len(skeys) == 0:
            return {'CANCELLED'}

        skey_names = sorted(skeys.keys(), key=lambda v: v.upper())

        for name in skey_names:
            idx = skeys.keys().index(name)
            ob.active_shape_key_index = idx
            bpy.ops.object.shape_key_move(type='BOTTOM')
        return {'FINISHED'}   

classes = (
    LAUNDRYBEAR_PT_ShapeKeyToolPanel,
    LAUNDRYBEAR_OT_ShapeKeySort,
)

def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)
  
def unregister():
    from bpy.utils import unregister_class
    for cls in reversed(classes):
        unregister_class(cls)
        
if __name__ == "__main__":
    register()