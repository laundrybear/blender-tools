import bpy
from . import LaundryBear

from bpy.app.handlers import persistent

@persistent #consider moving to timeline
def autoClamp_Framerange():
    context = bpy.context
    scene = context.scene
    obj = context.object
    aProps = scene.laundrybear_animation_properties

    if aProps.frameRange_auto == True:
        if obj.animation_data != None:
            if obj.animation_data.action != None:
                bpy.context.scene.frame_start = obj.animation_data.action.frame_range[0]
                bpy.context.scene.frame_end = obj.animation_data.action.frame_range[1]
    #repeat in seconds
    return 1.0


class LAUNDRYBEAR_PT_RigNamesPanel(bpy.types.Panel):
    bl_label = "Rig Names"
    bl_idname = "LAUNDRYBEAR_PT_rig_names"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Laundry Bear"
         
    def draw(self, context):
        layout = self.layout
        scene = context.scene
        aProps = scene.laundrybear_animation_properties
        layout.prop(aProps, "deform")
        layout.prop(aProps, "control")
        layout.prop(aProps, "face_mesh")
        layout.prop(aProps, "mesh_group")

class LAUNDRYBEAR_PT_AnimationToolPanel(bpy.types.Panel):
    bl_label = "Animation Tools"
    bl_idname = "LAUNDRYBEAR_PT_animation_tools"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Laundry Bear"
         
    def draw(self, context):
        layout = self.layout
        scene = context.scene
        aProps = scene.laundrybear_animation_properties
        
        row = layout.row()
        row.label(icon = "ARMATURE_DATA")
        row.operator("laundrybear.hop_to_control_rig")
        row = layout.row()
        row.label(icon = "BONE_DATA")
        row.operator("laundrybear.hop_to_deform_rig")

        row = layout.row()
        if aProps.rigHide_auto == True:
            row.label(icon = "HIDE_ON")
            row.prop(aProps, "rigHide_auto")
        else:
            row.label(icon = "HIDE_OFF")
            row.prop(aProps, "rigHide_auto")

        if aProps.rigEnable_auto == True:
            row.label(icon = "LOCKED")
            row.prop(aProps, "rigEnable_auto")
        else:
            row.label(icon = "UNLOCKED")
            row.prop(aProps, "rigEnable_auto")
            row = layout.row()
            row.label(icon = "LINKED")
            row.operator("laundrybear.enable_control_rig")
            row.label(icon = "UNLINKED")
            row.operator("laundrybear.disable_control_rig")

        row = layout.row()
        row.label(text = "Timeline Settings")
        row = layout.row()
        row.label(icon = "TIME")
        row.prop(aProps, "frameRange_auto")
      
class LAUNDRYBEAR_OT_EnableControlRig(bpy.types.Operator):
    """Enable Control Rigging Constraints"""
    bl_label = "Enable Control Rig"
    bl_idname = "laundrybear.enable_control_rig"
    
    #@classmethod
    #def poll(cls, context):
    #    scene = context.scene
    #    aProps = scene.laundrybear_animation_properties
    #    obj = context.object

    #    if obj.name == aProps.control:
    #        return False

    #    if obj != None:
    #        if obj.name == aProps.deform:
    #            return True
    #    return False
   
    def execute(self, context):
        scene = context.scene
        aProps = scene.laundrybear_animation_properties
        obj = context.object
        control = bpy.data.objects[aProps.control]

        #if obj != control:
        #    LaundryBear.setActivePoseMode(control, obj)

        LaundryBear.MuteControlRig(False)

        return {'FINISHED'}   

class LAUNDRYBEAR_OT_DisableControlRig(bpy.types.Operator):
    """Disable Control Rigging Constraints"""
    bl_label = "Disable Control Rig"
    bl_idname = "laundrybear.disable_control_rig"

    #@classmethod
    #def poll(cls, context):
    #    scene = context.scene
    #    aProps = scene.laundrybear_animation_properties
    #    obj = context.object

    #    if obj.name == aProps.control:
    #        return False

    #    if obj is not None:
    #        if obj.name == aProps.deform:
    #            return True
    #    return False
        
    def execute(self, context):
        scene = context.scene
        aProps = scene.laundrybear_animation_properties
        obj = context.object
        deform = bpy.data.objects[aProps.deform]

        #if obj != deform:
        #    LaundryBear.setActivePoseMode(deform, obj)

        LaundryBear.MuteControlRig(True)

        return {'FINISHED'}   

class LAUNDRYBEAR_OT_HopToControlRig(bpy.types.Operator):
    """Jump straight to Control Rig"""
    bl_label = "Jump to Control Rig"
    bl_idname = "laundrybear.hop_to_control_rig"
    
    @classmethod
    def poll(cls, context):
        scene = context.scene
        aProps = scene.laundrybear_animation_properties
        obj = context.object
        control = bpy.data.objects[aProps.control]

        if obj == control:
            return False

        return True
   
    def execute(self, context):
        scene = context.scene
        aProps = scene.laundrybear_animation_properties
        obj = context.object
        control = bpy.data.objects[aProps.control]
        deform = bpy.data.objects[aProps.deform]

        if obj != control:
            LaundryBear.setActivePoseMode(control, obj)

        if aProps.rigEnable_auto == True:
            LaundryBear.MuteControlRig(False)

        if aProps.rigHide_auto == True:
            deform.hide_set(True)

        return {'FINISHED'}   

class LAUNDRYBEAR_OT_HopToDeformRig(bpy.types.Operator):
    """Jump straight to Deform Rig"""
    bl_label = "Jump to Deform Rig"
    bl_idname = "laundrybear.hop_to_deform_rig"
    
    @classmethod
    def poll(cls, context):
        scene = context.scene
        aProps = scene.laundrybear_animation_properties
        obj = context.object
        deform = bpy.data.objects[aProps.deform]
        
        if obj == deform:
            return False
        return True
   
    def execute(self, context):
        scene = context.scene
        aProps = scene.laundrybear_animation_properties
        obj = context.object
        deform = bpy.data.objects[aProps.deform]
        control = bpy.data.objects[aProps.control]

        if obj != deform:
            LaundryBear.setActivePoseMode(deform, obj)
        
        if aProps.rigEnable_auto == True:
            LaundryBear.MuteControlRig(True)

        if aProps.rigHide_auto == True:
            control.hide_set(True)

        return {'FINISHED'}   


classes = (
    LAUNDRYBEAR_PT_RigNamesPanel,
    LAUNDRYBEAR_OT_EnableControlRig,
    LAUNDRYBEAR_OT_DisableControlRig,
    LAUNDRYBEAR_OT_HopToControlRig,
    LAUNDRYBEAR_OT_HopToDeformRig,
    LAUNDRYBEAR_PT_AnimationToolPanel,
)


def register():
    from bpy.utils import register_class
    for cls in classes:
        register_class(cls)
        #add animation property pointer to each class
        bpy.types.Scene.laundrybear_animation_properties = bpy.props.PointerProperty(type= LaundryBear.AnimationProperties)
    bpy.app.timers.register(autoClamp_Framerange)


def unregister():
    from bpy.utils import unregister_class
    for cls in reversed(classes):
        unregister_class(cls)
    bpy.app.timers.unregister(autoClamp_Framerange)    
if __name__ == "__main__":
    register()