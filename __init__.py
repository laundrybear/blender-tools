bl_info = {
    'name':'Laundry Bear Tools',
    'author': 'Fergui <forallthatneverwas@gmail.com>',
    'blender': (3, 0, 0),
    'version': (0, 0, 2),
    'description': "Game asset pipeline utilities",
    'location': "3D View > Side Panel > Laundry Bear",
    "tracker_url": "https://gitlab.com/laundrybear/blender-tools",
    'category': "System",
}

if "bpy" in locals():
    import importlib
    importlib.reload(LaundryBear)
    importlib.reload(AnimationTools)
    importlib.reload(BakeTools)
    importlib.reload(NLATools)
    importlib.reload(ShapeKeyTools)
else:
    import bpy

    from . import (
        LaundryBear,
        AnimationTools,
        BakeTools,
        NLATools,
        ShapeKeyTools,
    )

modules = (
    LaundryBear,
    AnimationTools,
    BakeTools,
    NLATools,
    ShapeKeyTools,
)

def register():
    for mod in modules:
        mod.register()
  


def unregister():
    for mod in reversed(modules):
        mod.unregister()
   
if __name__ == "__main__":
    register()