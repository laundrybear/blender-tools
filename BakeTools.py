import bpy
from . import LaundryBear

class LAUNDRYBEAR_PT_BakeToolPanel(bpy.types.Panel):
	bl_label = "Bake Tools"
	bl_idname = "LAUNDRYBEAR_PT_bake_tools"
	bl_space_type = "VIEW_3D"
	bl_region_type = "UI"
	bl_category = "Laundry Bear"

	def draw(self, context):
		layout = self.layout
		scene = context.scene
		aProps = scene.laundrybear_animation_properties
		row = layout.row()
		row.label(icon="SETTINGS")
		row.prop(aProps, "useNameBuilder")
		
		if aProps.useNameBuilder == True:
			row = layout.row()
			row.label(icon="MONKEY")
			row.prop(aProps, "actor")
			row = layout.row()
			row.label(icon="OUTLINER_COLLECTION")
			row.prop(aProps, "category")
			if aProps.category == 'Custom':
				row = layout.row()
				row.label(icon="GREASEPENCIL")
				row.prop(aProps, 'category_custom')
			row = layout.row()
			row.label(icon="TEXT")
			row.prop(aProps, "descriptor")
			row = layout.row()
			row.label(icon="LINENUMBERS_ON")
			row.prop(aProps, "instance")
			row.label(icon="MOD_MIRROR")
			row.prop(aProps, "side")
			row.label(icon="MOD_HUE_SATURATION")
			row.prop(aProps, "segment")
		else:
			row = layout.row()
			row.label(icon="GREASEPENCIL")
			row.prop(aProps, "name")

		row = layout.row()
		row.label(icon = "GROUP_BONE")
		row.operator("laundrybear.quick_bake")

		row = layout.row()
		row.label(text = "Export Settings")
		
		row = layout.row()
		row.label(icon="SHAPEKEY_DATA")
		row.prop(aProps, "export_blendshapes")
		row = layout.row()

		row.label(icon = "EXPORT")
		row.operator("laundrybear.quick_export")
		row = layout.row()
		row.label(icon = "EXPORT")
		row.operator("laundrybear.nla_export")


class LAUNDRYBEAR_OT_QuickBake(bpy.types.Operator):
	"""Bake current action to new Deform"""
	bl_label = "Quick Bake"
	bl_idname = "laundrybear.quick_bake"

	inBake = False

	@classmethod
	def poll(cls, context):
		scene = context.scene
		aProps = scene.laundrybear_animation_properties
		obj = context.object

		if obj.name == aProps.control:
			return True

		return False

	def execute(self, context):
		scene = context.scene
		aProps = scene.laundrybear_animation_properties
		obj = context.object

		# Constuct action name
		name = LaundryBear.buildName(self, context)
		print(name) 

		aProps.action_name = name
		LaundryBear.bakeAndPushDown(self, context)

		return {'FINISHED'}  

class LAUNDRYBEAR_OT_QuickExport(bpy.types.Operator):
	"""Bake and export current action to new fbx"""
	bl_label = "Export Single"
	bl_idname = "laundrybear.quick_export"

	@classmethod
	def poll(cls, context):
		scene = context.scene
		aProps = scene.laundrybear_animation_properties
		obj = context.object

		if obj.name == aProps.control:
			return True

		return False

	def execute(self, context):
		LaundryBear.exportFBX(self, context)
		return {'FINISHED'}   

class LAUNDRYBEAR_OT_NLAExport(bpy.types.Operator):
	"""Export active Deform NLA tracks to fbx"""
	bl_label = "Export Active NLA"
	bl_idname = "laundrybear.nla_export"

	@classmethod
	def poll(cls, context):
		scene = context.scene
		aProps = scene.laundrybear_animation_properties
		obj = context.object

		if obj.name == aProps.deform:
			return True

		return False

	def execute(self, context):
		LaundryBear.exportFBX(self, context)
		return {'FINISHED'}   

classes = (
	LAUNDRYBEAR_OT_QuickBake,
	LAUNDRYBEAR_OT_QuickExport,
	LAUNDRYBEAR_OT_NLAExport,
	LAUNDRYBEAR_PT_BakeToolPanel,
)


def register():
	from bpy.utils import register_class
	for cls in classes:
		register_class(cls)
		# add animation property pointer to each class
		bpy.types.Scene.laundrybear_animation_properties = bpy.props.PointerProperty(type= LaundryBear.AnimationProperties)


def unregister():
	from bpy.utils import unregister_class
	for cls in reversed(classes):
		unregister_class(cls) 

if __name__ == "__main__":
	register()