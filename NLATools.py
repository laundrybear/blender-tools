import bpy
from . import LaundryBear
from bpy.ops import (
	nla,
	anim,
)

class LAUNDRYBEAR_PT_NLAToolPanel(bpy.types.Panel):
	bl_label = "NLA Tools"
	bl_idname = "LAUNDRYBEAR_PT_nla_tools"
	bl_space_type = "NLA_EDITOR"
	bl_region_type = "UI"
	bl_category = "Laundry Bear"

	def draw(self, context):
		layout = self.layout
		scene = context.scene
		aProps = scene.laundrybear_animation_properties
		nla = scene.laundrybear_nla_properties

		row = layout.row()
		row.label(icon = "NLA_PUSHDOWN")
		row.operator("laundrybear.new_nla")

		row.label(icon="SETTINGS")
		row.prop(aProps, "useNameBuilder")
		
		if aProps.useNameBuilder == True:
			row = layout.row()
			row.label(icon="MONKEY")
			row.prop(aProps, "actor")
			row = layout.row()
			row.label(icon="OUTLINER_COLLECTION")
			row.prop(aProps, "category")
			if aProps.category == 'Custom':
				row = layout.row()
				row.label(icon="GREASEPENCIL")
				row.prop(aProps, 'category_custom')
			row = layout.row()
			row.label(icon="TEXT")
			row.prop(aProps, "descriptor")
			row = layout.row()
			row.label(icon="LINENUMBERS_ON")
			row.prop(aProps, "instance")
			row.label(icon="MOD_MIRROR")
			row.prop(aProps, "side")
			row.label(icon="MOD_HUE_SATURATION")
			row.prop(aProps, "segment")
		else:
			row = layout.row()
			row.label(icon="GREASEPENCIL")
			row.prop(aProps, "name")

		


	#NLA Category
		row = layout.row()
		row.label(text="Add Category")
		row = layout.row()
		row.label(icon="GREASEPENCIL")
		row.prop(nla, "category_marker")
		row = layout.row()
		row.prop(nla, "new_category")
		
		row.operator("laundrybear.nla_add_category")

	#NLA Sort
		row = layout.row()
		row.label(text="Sorting Tools")
		row = layout.row()
		row.label(icon="NLA")
		row.operator("laundrybear.nla_sort")

		row = layout.row()
		row.prop(context.scene, "category")

class LAUNDRYBEAR_OT_AddNLATrack(bpy.types.Operator):
	"""Create named NLA track"""
	bl_label = "New NLA Track"
	bl_idname = "laundrybear.new_nla"

	inBake = False

	@classmethod
	def poll(cls, context):
		scene = context.scene
		aProps = scene.laundrybear_animation_properties
		obj = context.object
			
		if obj.name == aProps.control:
			if scene.is_nla_tweakmode == True or obj.animation_data.action != None:
				return True

		return False

	def execute(self, context):
		scene = context.scene
		aProps = scene.laundrybear_animation_properties

		# Constuct action name
		name = LaundryBear.buildName(self, context)
		print(name) 

		aProps.action_name = name
		LaundryBear.pushDown(self, context)

		return {'FINISHED'}  	

class LAUNDRYBEAR_OT_NLATrackSort(bpy.types.Operator):
	"""Sort NLA Tracks By Category"""
	bl_label = "Categorize NLA Tracks"
	bl_idname = "laundrybear.nla_sort"

	@classmethod
	def poll(cls, context):
		scene = context.scene
		aProps = scene.laundrybear_animation_properties
		nlaProps = scene.laundrybear_nla_properties
		obj = context.object
		control = bpy.data.objects[aProps.control]
		deform = bpy.data.objects[aProps.deform]
		obj = context.object
		
		if obj is deform or obj is control:
			return True

		return False

	def execute(self, context):
		scene = context.scene
		#aProps = scene.laundrybear_animation_properties
		#nla = scene.laundrybear_nla_properties
		#control = bpy.data.objects[aProps.control]
		#deform = bpy.data.objects[aProps.deform]
		
		
		#Recreate Enum
		obj = context.object
		context.scene.category_collection.clear()
		
		for track in obj.animation_data.nla_tracks:
			if len(track.name.split('----')) > 1:
				Track_Category = context.scene.category_collection.add()
				Track_Category.name = track.name.split('----')[1]

		#Reorder tracks in Category
		order = 0
		tracks ={}
		for track in obj.animation_data.nla_tracks:
			tracks[order] = track.name
			order += 1

		ordered = dict(sorted(tracks.items(), key=lambda item: item[1]),reverse=True)
		print(ordered)

		indexed = []

		for key in ordered:
			indexed.append(key)

		index = 0
		while index < len(obj.animation_data.nla_tracks):
			bump = 0
			for track in obj.animation_data.nla_tracks:
				#print("track: " + str(track.name))
				#print("ordered: " + str(ordered[index]))
				if track.name == ordered[indexed[index]]:
					while bump > 0:
						#nla.click_select(wait_to_deselect_others=False, mouse_x=0, mouse_y=0, extend=False, deselect_all=False)
						nla.select_all(action = 'DESELECT')
						track.select = True
						#bpy.context.active_nla_track = track
						anim.channels_move(direction='DOWN')
						bump -= 1
					#obj.animation_data.nla_tracks[track.name].channel_index = index
					index += 1
				else:
					bump += 1
		return {'FINISHED'}   

class LAUNDRYBEAR_OT_AddNlaCategory(bpy.types.Operator):
	"""Sort NLA Tracks By Category"""
	bl_label = "Add NLA Category"
	bl_idname = "laundrybear.nla_add_category"

	@classmethod
	def poll(cls, context):
		scene = context.scene
		aProps = scene.laundrybear_animation_properties
		nla = scene.laundrybear_nla_properties
		obj = context.object
		control = bpy.data.objects[aProps.control]
		deform = bpy.data.objects[aProps.deform]
		obj = context.object

		if nla.new_category == "":
			return False
		else:
			for cat in context.scene.category_collection:
				if nla.new_category == cat.name:
					return False
		
		if obj is deform:
			return True

		if obj is control:
			return True

		return False

	def execute(self, context):
		scene = context.scene
		nla = scene.laundrybear_nla_properties
		obj = context.object
		obj = context.object

		new_track = obj.animation_data.nla_tracks.new()
		new_track.name = nla.category_marker + nla.new_category + nla.category_marker
		new_track.mute = True
		new_track.lock = True
		anim.channels_move(direction='DOWN')
		Track_Category = context.scene.category_collection.add()
		Track_Category.name = nla.new_category
		
		return {'FINISHED'}   

class Track_Category(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty()

classes = (
	Track_Category,
	LAUNDRYBEAR_OT_AddNLATrack,
	LAUNDRYBEAR_PT_NLAToolPanel,
	LAUNDRYBEAR_OT_NLATrackSort,
	LAUNDRYBEAR_OT_AddNlaCategory,
)



def categorize(self, context):
	Enum_items = []
	
	for category in context.scene.category_collection:
		
		name = category.name
		Track = (name, name, name)
		
		Enum_items.append(Track)
		
	return Enum_items

def register():
	from bpy.utils import register_class
	for cls in classes:
		register_class(cls)
		# add animation property pointer to each class
		bpy.types.Scene.laundrybear_animation_properties = bpy.props.PointerProperty(type= LaundryBear.AnimationProperties)
		bpy.types.Scene.laundrybear_nla_properties = bpy.props.PointerProperty(type= LaundryBear.NLAProperties)

		bpy.types.Scene.category = bpy.props.EnumProperty(items=categorize)
		bpy.types.Scene.category_collection = bpy.props.CollectionProperty(type=Track_Category)


def unregister():
	from bpy.utils import unregister_class
	for cls in reversed(classes):
		unregister_class(cls) 

if __name__ == "__main__":
	register()