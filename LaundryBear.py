#Laundry Bear Utilities
import bpy
from bpy.ops import (
	nla,
	anim,
)

#Property Groups
class AnimationProperties(bpy.types.PropertyGroup):
	deform : bpy.props.StringProperty(name= "Deform", default= "Armature_Deform")
	control : bpy.props.StringProperty(name= "Control", default= "Armature_Control")
	face_mesh : bpy.props.StringProperty(name= "Face Mesh", default= "Head")
	mesh_group : bpy.props.StringProperty(name= "Mesh Group", default= "Mesh Group")
	action_name : bpy.props.StringProperty(name= "Action Name", description= "Internal name for action", default= "Action")
	useNameBuilder : bpy.props.BoolProperty(name= "Use Name Builder", default= True)
	name : bpy.props.StringProperty(name= "Name", description= "Manual name using Actor|Category_Description_Instance_Side_Segment", default= "Player|Idle_Combat_01_Left")
	actor : bpy.props.StringProperty(name= "Actor", description= "The name of the character/creature/object",default= "Player")
	category : bpy.props.EnumProperty(
		name= "Category",
		description= "collection of actions to be part of. Use 'Custom' if there is no suitable category.",
		items= [('Idle', "Idle", ""),
				('Combat', "Combat", ""),
				('Locomotion', "Locomotion", ""),
				('Promo', "Promo", ""),
				('Dialogue', "Dialogue", ""),
				('TOS', "TOS", ""),
				('Spawn', "Spawn", ""),
				('Death', "Death", ""),
				('Item', "Item", ""),
				('Interact', "Interact", ""),
				('Effect', "Effect", ""),
				('Skill', "Skill", ""),
				('Spell', "Spell", ""),
				('PowerUp', "PowerUp", ""),
				('Taunt', "Taunt", ""),
				('React', "React", ""),
				('UpperBody', "Upperbody", ""),
				('LowerBody', "LowerBody", ""),
				('Head', "Head", ""),
				('Hand', "Hand", ""),
				('Position', "Position", ""),
				('Misc', "Misc", ""),
				('Test', "Test", ""),
				('Custom', "Custom", ""),
		],
	)
	category_custom : bpy.props.StringProperty(name= "Category", description= "Unlisted Category", default= "Custom")
	descriptor : bpy.props.StringProperty(name= "Descriptor", description= "Name of the actual action in category. Ie Idle_'Combat' Combat_'Combo', or Locomotion_'Jump'",default= "Combat")
	instance : bpy.props.IntProperty(name= "Instance", description= "Number in set. Use 0 to not number", default= 1, soft_min= 0)
	side : bpy.props.EnumProperty(
		name= "Side",
		description= "None if not a left/right variant",
		items= [('None', "None", ""),
				('Left', "Left", ""),
				('Right', "Right", ""),
		],
	)
	segment : bpy.props.EnumProperty(
		name= "Segment",
		description= "None if not part of a multi-action animation",
		items= [('None', "None", ""),
				('In', "In", ""),
				('Loop', "Loop", ""),
				('Out', "Out", "")
		],
	)
	frameRange_auto : bpy.props.BoolProperty(name= "Auto-clamp Frame Range", default= True)
	rigHide_auto : bpy.props.BoolProperty(name= "Hide other Rig", default= False)
	rigEnable_auto : bpy.props.BoolProperty(name= "Lock Rig to Jump", default= False)

	export_blendshapes : bpy.props.BoolProperty(name= "Export BlendShapes", default= False)

class NLAProperties(bpy.types.PropertyGroup):
	export_category : bpy.props.StringProperty(name= "Export Category", default= "Idle")
	category_marker : bpy.props.StringProperty(name= "Category Marker", default= "----")
	new_category : bpy.props.StringProperty(name= "Name", default= "")
	categories = []
	category : bpy.props.EnumProperty(
		name = "Category",
		description = "Category to export or clean",
		items = categories
	)

#Functions
def MuteControlRig (mute: bool):
		scene = bpy.context.scene
		aProps = scene.laundrybear_animation_properties
		obj = bpy.data.objects[aProps.deform]
		constraints = [c for pb in obj.pose.bones 
						 for c in pb.constraints
					  ]
	   
		for c in constraints:
			c.mute = mute

def setActivePoseMode(target: object, current: object = None, hide: bool = None):
	context = bpy.context

	if current is None:
		current = context.object

	if current.mode != 'OBJECT':
		bpy.ops.object.mode_set(mode='OBJECT', toggle=True)
	bpy.ops.object.select_all(action = 'DESELECT')
	target.hide_set(False)
	target.select_set(True)
	context.view_layer.objects.active = target
	bpy.ops.object.mode_set(mode='POSE', toggle=True)

	if hide is not None:
		current.hide_set(hide)

def buildName(self, context):
		scene = context.scene
		aProps = scene.laundrybear_animation_properties
		obj = context.object

		name = ""

		if aProps.useNameBuilder == True:
			name += aProps.actor + '|'

			if aProps.category == 'Custom':
				name += aProps.category_custom
			else:
				name += aProps.category
			
			name += '_' + aProps.descriptor
			
			if aProps.instance >= 1:
				name += '_' + "%02d" % aProps.instance
			
			if aProps.side != 'None':
				name += '_' + aProps.side

			if aProps.segment != 'None':
				name += '_' + aProps.segment
		else:
			name = aProps.name
		return name 

def pushDown(self, context):
	scene = context.scene
	aProps = scene.laundrybear_animation_properties

	name = aProps.action_name

	deform = bpy.data.objects[aProps.deform]
	control = bpy.data.objects[aProps.control]

	#Bake and pushdown the action
	control_action = control.animation_data.action

	control.animation_data.use_nla = True

	new_track = control.animation_data.nla_tracks.new()
	new_track.name = name.split('|')[1]
	new_track.strips.new(name, control_action.frame_range[0], control_action)
	control.animation_data.action.use_fake_user = True

	if scene.is_nla_tweakmode == True:
		nla.tweakmode_exit()
	else:
		control.animation_data.action.unlink()


def bakeAndPushDown(self, context):
	scene = context.scene
	aProps = scene.laundrybear_animation_properties

	name = aProps.action_name

	#make Deform active
	deform = bpy.data.objects[aProps.deform]
	control = bpy.data.objects[aProps.control]

	setActivePoseMode(deform, control)

	#Bake and pushdown the action
	control_action = control.animation_data.action
	bpy.ops.nla.bake(
		frame_start= control_action.frame_range[0], 
		frame_end= control_action.frame_range[1], 
		step=1, 
		only_selected=False, 
		visual_keying=True, 
		clear_constraints=False, 
		clear_parents=False, 
		use_current_action=False, 
		clean_curves=False, 
		bake_types={'POSE'}
	)
	new_action = deform.animation_data.action
	new_action.name = name
	new_action.use_fake_user = True

	deform.animation_data.use_nla = True

	for track in deform.animation_data.nla_tracks:
		track.mute = True

	new_track = deform.animation_data.nla_tracks.new()
	new_track.name = new_action.name.split('|')[1]
	new_track.strips.new(new_action.name, new_action.frame_range[0], new_action)
	deform.animation_data.action = None
	
def exportFBX(self, context):
	scene = context.scene
	aProps = scene.laundrybear_animation_properties
	obj = context.object
	
	deform = bpy.data.objects[aProps.deform]
	control = bpy.data.objects[aProps.control]

	# Constuct action name
	name = buildName(self, context)
	print(name) 

	aProps.action_name = name

	#if active object is Control, then bake and switch to deform
	if obj.name == aProps.control:
		print("exporting single")
		bakeAndPushDown(self, context)
		MuteControlRig(True)
		
	else:
		print("exporting active")

	mesh_group = bpy.data.objects[aProps.mesh_group]
	mesh_group.select_set(True)

	if aProps.export_blendshapes == True:
		face_mesh = bpy.data.objects[aProps.face_mesh]
		face_mesh.select_set(True)

	bpy.ops.object.mode_set(mode='OBJECT', toggle=True)

	bpy.ops.export_scene.fbx('INVOKE_DEFAULT',
		filepath= bpy.path.abspath("//{}.fbx".format(name)),
		check_existing=True, 
		use_selection=True, 
		use_active_collection=False, 
		global_scale=1.0, 
		apply_unit_scale=True, 
		apply_scale_options='FBX_SCALE_ALL', 
		use_space_transform=True, 
		bake_space_transform=True, 
		object_types={'ARMATURE', 'EMPTY', 'MESH', 'OTHER'}, 
		use_mesh_modifiers=True, 
		use_mesh_modifiers_render=True, 
		mesh_smooth_type='FACE', 
		use_subsurf=False, 
		use_mesh_edges=False, 
		use_tspace=False, 
		use_custom_props=False, 
		add_leaf_bones=False, 
		primary_bone_axis='Y', 
		secondary_bone_axis='X', 
		use_armature_deform_only=False, 
		armature_nodetype='NULL', 
		bake_anim=True, 
		bake_anim_use_all_bones=True, 
		bake_anim_use_nla_strips=True, 
		bake_anim_use_all_actions=False, 
		bake_anim_force_startend_keying=True, 
		bake_anim_step=2.0, 
		bake_anim_simplify_factor=0.0, 
		path_mode='AUTO', 
		embed_textures=False, 
		batch_mode='OFF', 
		use_batch_own_dir=True, 
		use_metadata=True, 
		axis_forward='-Z', 
		axis_up='Y') 


classes = (
	AnimationProperties,
	NLAProperties,
)


def register():
	from bpy.utils import register_class
	for cls in classes:
		register_class(cls)
		

def unregister():
	#deleting the properties doesn't require deleting each pointer
	del bpy.types.Scene.laundrybear_animation_properties 
	del bpy.types.Scene.laundrybear_nla_properties

	from bpy.utils import unregister_class
	for cls in reversed(classes):
		unregister_class(cls)

if __name__ == "__main__":
	register()